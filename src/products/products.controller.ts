import {
  Controller,
  Get,
  Post,
  Body,
  Patch,
  Param,
  Delete,
  UseInterceptors,
  UploadedFile,
  Res,
  Query,
} from '@nestjs/common';
import { ProductsService } from './products.service';
import { CreateProductDto } from './dto/create-product.dto';
import { UpdateProductDto } from './dto/update-product.dto';
import { FileInterceptor } from '@nestjs/platform-express';
import { v4 as uuidv4 } from 'uuid';
import { diskStorage } from 'multer';
import { extname } from 'path';
import { Response, query } from 'express';

@Controller('products')
export class ProductsController {
  constructor(private readonly productsService: ProductsService) {}

  @Post()
  @UseInterceptors(
    FileInterceptor('file', {
      storage: diskStorage({
        destination: './product_images',
        filename: (reg, file, cb) => {
          const name = uuidv4();
          return cb(null, name + extname(file.originalname));
        },
      }),
    }),
  )
  create(
    @Body() createProductDto: CreateProductDto,
    @UploadedFile() file: Express.Multer.File,
  ) {
    createProductDto.image = file.filename;
    return this.productsService.create(createProductDto);
  }

  @Get('category/:id')
  findByCategory(@Param('id') id: string) {
    return this.productsService.findByCategory(+id);
  }

  // @Get()
  // findAll() {
  //   return this.productsService.findAll();
  // }

  @Get()
  findAll(@Query() query: { cat?: string; order?: string; orderBy?: string }) {
    return this.productsService.findAll({
      relations: ['category'],
      order: query.orderBy
        ? { [query.orderBy]: query.order }
        : { createdAt: 'ASC' },
      where: query.cat ? { categoryId: parseInt(query.cat) } : {},
    });
  }

  @Get(':id')
  findOne(@Param('id') id: string) {
    return this.productsService.findOne(+id);
  }

  @Get(':id/image')
  async getImage(@Param('id') id: string, @Res() res: Response) {
    const product = await this.productsService.findOne(+id);
    res.sendFile(product.image, { root: './product_images' });
  }

  @Get('image/:imageFile')
  async getImageByFileName(
    @Param('imageFile') imageFile: string,
    @Res() res: Response,
  ) {
    res.sendFile(imageFile, { root: './product_images' });
  }

  @Patch(':id')
  updateImage(
    @Param('id') id: string,
    @Body() updateProductDto: UpdateProductDto,
  ) {
    return this.productsService.update(+id, updateProductDto);
  }

  @Patch(':id/image')
  @UseInterceptors(
    FileInterceptor('file', {
      storage: diskStorage({
        destination: './product_images',
        filename: (reg, file, cb) => {
          const name = uuidv4();
          return cb(null, name + extname(file.originalname));
        },
      }),
    }),
  )
  update(@Param('id') id: string, @UploadedFile() file: Express.Multer.File) {
    return this.productsService.update(+id, { image: file.filename });
  }

  @Delete(':id')
  remove(@Param('id') id: string) {
    return this.productsService.remove(+id);
  }

  // @Get('/product')
  // getProduct(@Query()) {
  //   return this.productsService.d;
  // }

  // @Get('/product')
  // getProduct(@Query() query: { searchText?: string }) {
  //   console.log(query.searchText);
  //   if (query.searchText) {
  //     return this.productsService.getProductBySearchText(query.searchText);
  //   }
  //   return this.productsService.getProduct();
  // }
}
