import { IsNotEmpty, Length, IsNumber, Min } from 'class-validator';

export class CreateProductDto {
  @IsNotEmpty()
  @Length(3, 32)
  name: string;

  // @IsNotEmpty()
  // type: string;

  @IsNotEmpty()
  price: number;

  image = 'No img product.png';

  @IsNotEmpty()
  categoryId: number;
}
