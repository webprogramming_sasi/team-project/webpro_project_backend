import { Injectable, NotFoundException } from '@nestjs/common';
import { CreateEmployeeDto } from './dto/create-employee.dto';
import { UpdateEmployeeDto } from './dto/update-employee.dto';
import { InjectRepository } from '@nestjs/typeorm';
import { Employee } from './entities/employee.entity';
import { Repository } from 'typeorm';

@Injectable()
export class EmployeesService {
  constructor(
    @InjectRepository(Employee)
    private employeeRepository: Repository<Employee>,
  ) {}

  create(createEmployeeDto: CreateEmployeeDto) {
    return this.employeeRepository.save(createEmployeeDto);
  }

  findAll() {
    return this.employeeRepository.find();
  }

  findOne(id: number) {
    return this.employeeRepository.findOne({ where: { id: id } });
  }

  findOneByUsername(username: string) {
    return this.employeeRepository.findOne({ where: { username: username } });
  }

  async update(id: number, updateEmployeeDto: UpdateEmployeeDto) {
    try {
      const updateEmployee = await this.employeeRepository.save({
        id,
        ...updateEmployeeDto,
      });
      return updateEmployee;
    } catch (e) {
      throw new NotFoundException();
    }
  }

  async remove(id: number) {
    const employee = await this.employeeRepository.findOne({
      where: { id: id },
    });
    try {
      const deleteEmployee = await this.employeeRepository.remove(employee);
      return deleteEmployee;
    } catch (e) {
      throw new NotFoundException();
    }
  }
}
