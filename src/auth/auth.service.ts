import { Injectable } from '@nestjs/common';
import { EmployeesService } from '../employees/employees.service';
import { JwtService } from '@nestjs/jwt';
import { Employee } from 'src/employees/entities/employee.entity';

@Injectable()
export class AuthService {
  constructor(
    private employeesService: EmployeesService,
    private jwtService: JwtService,
  ) {}

  async validateUser(user: string, pass: string): Promise<any> {
    const employee = await this.employeesService.findOneByUsername(user);
    if (employee && employee.password === pass) {
      const { password, ...result } = employee;
      return result;
    }
    return null;
  }

  async login(employee: Employee) {
    const payload = { user: employee.username, sub: employee.id };
    return {
      employee: employee,
      access_token: this.jwtService.sign(payload),
    };
  }
}
