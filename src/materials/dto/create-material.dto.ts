import { IsNotEmpty } from 'class-validator';

export class CreateMaterialDto {
  @IsNotEmpty()
  name: string;

  @IsNotEmpty()
  minimum: number;

  @IsNotEmpty()
  remain: number;

  @IsNotEmpty()
  unit: string;
}
