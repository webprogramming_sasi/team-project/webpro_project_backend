import { OrderItem } from 'src/orders/entities/order-item';
import { ServQueue } from 'src/serv-queues/entities/serv-queue.entity';
import {
  Column,
  Entity,
  ManyToOne,
  OneToMany,
  PrimaryGeneratedColumn,
} from 'typeorm';

@Entity()
export class FoodQueue {
  @PrimaryGeneratedColumn()
  id: number;

  @Column()
  name: string;

  @Column()
  quantity: number;

  @Column()
  status: string;

  @ManyToOne(() => OrderItem, (orderItemEntity) => orderItemEntity.foodQueue)
  orderItem: OrderItem;

  @OneToMany(() => ServQueue, (servQueueEntity) => servQueueEntity.foodQueue)
  servQueue: ServQueue;
}
