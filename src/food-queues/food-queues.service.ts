import { Injectable, NotFoundException } from '@nestjs/common';
import { CreateFoodQueueDto } from './dto/create-food-queue.dto';
import { UpdateFoodQueueDto } from './dto/update-food-queue.dto';
import { InjectRepository } from '@nestjs/typeorm';
import { FoodQueue } from './entities/food-queue.entity';
import { Repository } from 'typeorm';

@Injectable()
export class FoodQueuesService {
  constructor(
    @InjectRepository(FoodQueue)
    private foodQueueRepository: Repository<FoodQueue>,
  ) {}

  create(createFoodQueueDto: CreateFoodQueueDto) {
    return this.foodQueueRepository.save(createFoodQueueDto);
  }

  findAll() {
    return this.foodQueueRepository.find();
  }

  findOne(id: number) {
    return this.foodQueueRepository.findOne({ where: { id: id } });
  }

  async update(id: number, updateFoodQueueDto: UpdateFoodQueueDto) {
    try {
      const updateFoodQueue = await this.foodQueueRepository.save({
        id,
        ...updateFoodQueueDto,
      });
      return updateFoodQueue;
    } catch (e) {
      throw new NotFoundException();
    }
  }

  async remove(id: number) {
    const foodQueue = await this.foodQueueRepository.findOne({
      where: { id: id },
    });
    try {
      const deleteFoodQueue = await this.foodQueueRepository.remove(foodQueue);
      return deleteFoodQueue;
    } catch (e) {
      throw new NotFoundException();
    }
  }
}
