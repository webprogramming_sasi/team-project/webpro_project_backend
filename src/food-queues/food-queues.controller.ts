import {
  Controller,
  Get,
  Post,
  Body,
  Patch,
  Param,
  Delete,
} from '@nestjs/common';
import { FoodQueuesService } from './food-queues.service';
import { CreateFoodQueueDto } from './dto/create-food-queue.dto';
import { UpdateFoodQueueDto } from './dto/update-food-queue.dto';

@Controller('food-queues')
export class FoodQueuesController {
  constructor(private readonly foodQueuesService: FoodQueuesService) {}

  @Post()
  create(@Body() createFoodQueueDto: CreateFoodQueueDto) {
    return this.foodQueuesService.create(createFoodQueueDto);
  }

  @Get()
  findAll() {
    return this.foodQueuesService.findAll();
  }

  @Get(':id')
  findOne(@Param('id') id: string) {
    return this.foodQueuesService.findOne(+id);
  }

  @Patch(':id')
  update(
    @Param('id') id: string,
    @Body() updateFoodQueueDto: UpdateFoodQueueDto,
  ) {
    return this.foodQueuesService.update(+id, updateFoodQueueDto);
  }

  @Delete(':id')
  remove(@Param('id') id: string) {
    return this.foodQueuesService.remove(+id);
  }
}
