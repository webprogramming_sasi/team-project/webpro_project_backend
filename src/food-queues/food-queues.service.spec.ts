import { Test, TestingModule } from '@nestjs/testing';
import { FoodQueuesService } from './food-queues.service';

describe('FoodQueuesService', () => {
  let service: FoodQueuesService;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      providers: [FoodQueuesService],
    }).compile();

    service = module.get<FoodQueuesService>(FoodQueuesService);
  });

  it('should be defined', () => {
    expect(service).toBeDefined();
  });
});
