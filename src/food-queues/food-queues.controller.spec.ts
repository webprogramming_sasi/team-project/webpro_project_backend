import { Test, TestingModule } from '@nestjs/testing';
import { FoodQueuesController } from './food-queues.controller';
import { FoodQueuesService } from './food-queues.service';

describe('FoodQueuesController', () => {
  let controller: FoodQueuesController;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      controllers: [FoodQueuesController],
      providers: [FoodQueuesService],
    }).compile();

    controller = module.get<FoodQueuesController>(FoodQueuesController);
  });

  it('should be defined', () => {
    expect(controller).toBeDefined();
  });
});
