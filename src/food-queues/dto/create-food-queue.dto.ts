export class CreateFoodQueueDto {
  name: string;
  quantity: number;
  status: string;
}
