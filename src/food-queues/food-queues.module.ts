import { Module } from '@nestjs/common';
import { FoodQueuesService } from './food-queues.service';
import { FoodQueuesController } from './food-queues.controller';
import { TypeOrmModule } from '@nestjs/typeorm';
import { FoodQueue } from './entities/food-queue.entity';

@Module({
  imports: [TypeOrmModule.forFeature([FoodQueue])],
  controllers: [FoodQueuesController],
  providers: [FoodQueuesService],
})
export class FoodQueuesModule {}
