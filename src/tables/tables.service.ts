import { Injectable, NotFoundException } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository } from 'typeorm';
import { CreateTableDto } from './dto/create-table.dto';
import { UpdateTableDto } from './dto/update-table.dto';
import { Table } from './entities/table.entity';

@Injectable()
export class TablesService {
  constructor(
    @InjectRepository(Table) private tablesRepository: Repository<Table>,
  ) {}

  create(createTableDto: CreateTableDto) {
    return this.tablesRepository.save(createTableDto);
  }

  findAll() {
    return this.tablesRepository.find();
  }

  findOne(id: number) {
    return this.tablesRepository.findOne({ where: { id } });
  }

  async update(id: number, updateTableDto: UpdateTableDto) {
    const table = await this.tablesRepository.findOneBy({ id });
    if (!table) {
      throw new NotFoundException();
    }
    const updatedTable = { ...table, ...updateTableDto };

    return this.tablesRepository.save(updatedTable);
  }

  async remove(id: number) {
    const table = await this.tablesRepository.findOneBy({ id });
    if (!table) {
      throw new NotFoundException();
    }
    return this.tablesRepository.softRemove(table);
  }
}
