import { IsInt, IsNotEmpty, Min } from 'class-validator';

export class CreateTableDto {
  @IsNotEmpty()
  number: number;

  @IsInt({ message: 'Max seat values must be an integer.' })
  @Min(0, { message: 'Max seat values cant be less than "0".' })
  seats: number;

  @IsNotEmpty()
  status: string;
}
