import { Injectable, NotFoundException } from '@nestjs/common';
import { CreateRoleDto } from './dto/create-role.dto';
import { UpdateRoleDto } from './dto/update-role.dto';
import { Injector } from '@nestjs/core/injector/injector';
import { Role } from './entities/role.entity';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository } from 'typeorm';

@Injectable()
export class RolesService {
  constructor(
    @InjectRepository(Role)
    private roleRepository: Repository<Role>,
  ) {}

  create(createRoleDto: CreateRoleDto) {
    return this.roleRepository.save(createRoleDto);
  }

  findAll() {
    return this.roleRepository.find();
  }

  findOne(id: number) {
    return this.roleRepository.findOne({ where: { id: id } });
  }

  async update(id: number, updateRoleDto: UpdateRoleDto) {
    try {
      const updatedRole = await this.roleRepository.save({
        id,
        ...updateRoleDto,
      });
      return updatedRole;
    } catch (e) {
      throw new NotFoundException();
    }
  }

  async remove(id: number) {
    const role = await this.roleRepository.findOne({
      where: { id: id },
    });
    try {
      const deletedRole = await this.roleRepository.remove(role);
      return deletedRole;
    } catch (e) {
      throw new NotFoundException();
    }
  }
}
