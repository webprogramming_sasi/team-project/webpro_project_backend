export class CreateReceiptDto {
  // emp_id: number:
  // order_id: number;

  discount: number;

  tax: number;

  total: number;

  payment: string;

  cash: number;

  change: number;

  date: Date;
}
