import { Injectable, NotFoundException } from '@nestjs/common';
import { CreateReceiptDto } from './dto/create-receipt.dto';
import { UpdateReceiptDto } from './dto/update-receipt.dto';
import { InjectRepository } from '@nestjs/typeorm';
import { Receipt } from './entities/receipt.entity';
import { Repository } from 'typeorm';

@Injectable()
export class ReceiptsService {
  constructor(
    @InjectRepository(Receipt)
    private recieptRepository: Repository<Receipt>,
  ) {}

  create(createReceiptDto: CreateReceiptDto) {
    return this.recieptRepository.save(createReceiptDto);
  }

  findAll() {
    return this.recieptRepository.find({ relations: ['order', 'employee'] });
  }

  findOne(id: number) {
    return this.recieptRepository.findOne({ where: { id: id } });
  }

  async update(id: number, updateReceiptDto: UpdateReceiptDto) {
    try {
      const updatedReciept = await this.recieptRepository.save({
        id,
        ...updateReceiptDto,
      });
      return updatedReciept;
    } catch (e) {
      throw new NotFoundException();
    }
  }

  async remove(id: number) {
    const reciept = await this.recieptRepository.findOne({
      where: { id: id },
    });
    try {
      const deletedReciept = await this.recieptRepository.remove(reciept);
      return deletedReciept;
    } catch (e) {
      throw new NotFoundException();
    }
  }
}
