import { Employee } from 'src/employees/entities/employee.entity';
import { Order } from 'src/orders/entities/order.entity';
import {
  Column,
  CreateDateColumn,
  Entity,
  ManyToOne,
  PrimaryGeneratedColumn,
} from 'typeorm';

@Entity()
export class Receipt {
  @PrimaryGeneratedColumn()
  id: number;
  // emp_id: number:
  // order_id: number;
  @Column()
  discount: number;

  @Column()
  tax: number;

  @Column()
  total: number;

  @Column()
  payment: string;

  @Column()
  cash: number;

  @Column()
  change: number;

  @CreateDateColumn()
  date: Date;

  @ManyToOne(() => Order, (order) => order.reciept)
  order: Order[];

  @ManyToOne(() => Employee, (employeeEntity) => employeeEntity.receipt)
  employee: Employee;
}
