import { Test, TestingModule } from '@nestjs/testing';
import { ServQueuesController } from './serv-queues.controller';
import { ServQueuesService } from './serv-queues.service';

describe('ServQueuesController', () => {
  let controller: ServQueuesController;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      controllers: [ServQueuesController],
      providers: [ServQueuesService],
    }).compile();

    controller = module.get<ServQueuesController>(ServQueuesController);
  });

  it('should be defined', () => {
    expect(controller).toBeDefined();
  });
});
