import {
  Controller,
  Get,
  Post,
  Body,
  Patch,
  Param,
  Delete,
} from '@nestjs/common';
import { ServQueuesService } from './serv-queues.service';
import { CreateServQueueDto } from './dto/create-serv-queue.dto';
import { UpdateServQueueDto } from './dto/update-serv-queue.dto';

@Controller('serv-queues')
export class ServQueuesController {
  constructor(private readonly servQueuesService: ServQueuesService) {}

  @Post()
  create(@Body() createServQueueDto: CreateServQueueDto) {
    return this.servQueuesService.create(createServQueueDto);
  }

  @Get()
  findAll() {
    return this.servQueuesService.findAll();
  }

  @Get(':id')
  findOne(@Param('id') id: string) {
    return this.servQueuesService.findOne(+id);
  }

  @Patch(':id')
  update(
    @Param('id') id: string,
    @Body() updateServQueueDto: UpdateServQueueDto,
  ) {
    return this.servQueuesService.update(+id, updateServQueueDto);
  }

  @Delete(':id')
  remove(@Param('id') id: string) {
    return this.servQueuesService.remove(+id);
  }
}
