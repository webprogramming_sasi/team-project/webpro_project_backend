import { Module } from '@nestjs/common';
import { ServQueuesService } from './serv-queues.service';
import { ServQueuesController } from './serv-queues.controller';
import { TypeOrmModule } from '@nestjs/typeorm';
import { ServQueue } from './entities/serv-queue.entity';

@Module({
  imports: [TypeOrmModule.forFeature([ServQueue])],
  controllers: [ServQueuesController],
  providers: [ServQueuesService],
})
export class ServQueuesModule {}
