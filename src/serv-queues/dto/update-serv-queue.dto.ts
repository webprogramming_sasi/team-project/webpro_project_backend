import { PartialType } from '@nestjs/mapped-types';
import { CreateServQueueDto } from './create-serv-queue.dto';

export class UpdateServQueueDto extends PartialType(CreateServQueueDto) {}
