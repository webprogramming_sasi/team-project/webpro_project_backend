import { Test, TestingModule } from '@nestjs/testing';
import { ServQueuesService } from './serv-queues.service';

describe('ServQueuesService', () => {
  let service: ServQueuesService;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      providers: [ServQueuesService],
    }).compile();

    service = module.get<ServQueuesService>(ServQueuesService);
  });

  it('should be defined', () => {
    expect(service).toBeDefined();
  });
});
