import { FoodQueue } from 'src/food-queues/entities/food-queue.entity';
import { Column, Entity, ManyToOne, PrimaryGeneratedColumn } from 'typeorm';

@Entity()
export class ServQueue {
  @PrimaryGeneratedColumn()
  id: number;
  // foodqueue: number;

  @Column()
  status: string;

  @ManyToOne(() => FoodQueue, (foodQueueEntity) => foodQueueEntity.servQueue)
  foodQueue: FoodQueue;
}
