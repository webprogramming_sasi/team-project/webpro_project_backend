import { Injectable, NotFoundException } from '@nestjs/common';
import { CreateServQueueDto } from './dto/create-serv-queue.dto';
import { UpdateServQueueDto } from './dto/update-serv-queue.dto';
import { InjectRepository } from '@nestjs/typeorm';
import { ServQueue } from './entities/serv-queue.entity';
import { Repository } from 'typeorm';

@Injectable()
export class ServQueuesService {
  constructor(
    @InjectRepository(ServQueue)
    private servQueueRepository: Repository<ServQueue>,
  ) {}

  create(createServQueueDto: CreateServQueueDto) {
    return this.servQueueRepository.save(createServQueueDto);
  }

  findAll() {
    return this.servQueueRepository.find({
      relations: ['foodQueue'],
    });
  }

  findOne(id: number) {
    return this.servQueueRepository.findOne({
      where: { id: id },
      relations: ['foodQueue'],
    });
  }

  async update(id: number, updateServQueueDto: UpdateServQueueDto) {
    try {
      const updatedServQueue = await this.servQueueRepository.save({
        id,
        ...updateServQueueDto,
      });
      return updatedServQueue;
    } catch (e) {
      throw new NotFoundException();
    }
  }

  async remove(id: number) {
    const ServQueue = await this.servQueueRepository.findOne({
      where: { id: id },
    });
    try {
      const deletedServQueue = await this.servQueueRepository.remove(ServQueue);
      return deletedServQueue;
    } catch (e) {
      throw new NotFoundException();
    }
  }
}
