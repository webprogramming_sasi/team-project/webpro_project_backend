import { Module } from '@nestjs/common';
import { DessertsService } from './desserts.service';
import { DessertsController } from './desserts.controller';
import { Dessert } from './entities/dessert.entity';
import { TypeOrmModule } from '@nestjs/typeorm';
@Module({
  imports: [TypeOrmModule.forFeature([Dessert])],
  controllers: [DessertsController],
  providers: [DessertsService],
})
export class DessertsModule {}
