import { IsNotEmpty, Length, IsNumber, Min } from 'class-validator';

export class CreateDessertDto {
  @IsNotEmpty()
  @Length(3, 32)
  name: string;

  @IsNotEmpty()
  type: string;

  @IsNumber()
  @Min(0)
  price: number;
}
