import {
  Controller,
  Get,
  Post,
  Body,
  Patch,
  Param,
  Delete,
} from '@nestjs/common';
import { DessertsService } from './desserts.service';
import { CreateDessertDto } from './dto/create-dessert.dto';
import { UpdateDessertDto } from './dto/update-dessert.dto';

@Controller('desserts')
export class DessertsController {
  constructor(private readonly dessertsService: DessertsService) {}

  @Post()
  create(@Body() createDessertDto: CreateDessertDto) {
    return this.dessertsService.create(createDessertDto);
  }

  @Get()
  findAll() {
    return this.dessertsService.findAll();
  }

  @Get(':id')
  findOne(@Param('id') id: string) {
    return this.dessertsService.findOne(+id);
  }

  @Patch(':id')
  update(@Param('id') id: string, @Body() updateDessertDto: UpdateDessertDto) {
    return this.dessertsService.update(+id, updateDessertDto);
  }

  @Delete(':id')
  remove(@Param('id') id: string) {
    return this.dessertsService.remove(+id);
  }
}
