import { Injectable, NotFoundException } from '@nestjs/common';
import { CreateDessertDto } from './dto/create-dessert.dto';
import { UpdateDessertDto } from './dto/update-dessert.dto';
import { Dessert } from './entities/dessert.entity';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository } from 'typeorm';

@Injectable()
export class DessertsService {
  constructor(
    @InjectRepository(Dessert)
    private dessertsRepository: Repository<Dessert>,
  ) {}
  create(createDessertDto: CreateDessertDto) {
    return this.dessertsRepository.save(createDessertDto);
  }

  findAll() {
    return this.dessertsRepository.find();
  }

  findOne(id: number) {
    return this.dessertsRepository.findOne({ where: { id: id } });
  }

  async update(id: number, updateDessertDto: UpdateDessertDto) {
    try {
      const updatedProduct = await this.dessertsRepository.save({
        id,
        ...updateDessertDto,
      });
      return updatedProduct;
    } catch (e) {
      throw new NotFoundException();
    }
  }

  async remove(id: number) {
    const dessert = await this.dessertsRepository.findOne({
      where: { id: id },
    });
    try {
      const deletedDessert = await this.dessertsRepository.remove(dessert);
      return deletedDessert;
    } catch (e) {
      throw new NotFoundException();
    }
  }
}
