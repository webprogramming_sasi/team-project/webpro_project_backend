import { Module } from '@nestjs/common';
import { TypeOrmModule } from '@nestjs/typeorm';
import { DataSource } from 'typeorm';
import { AppController } from './app.controller';
import { AppService } from './app.service';
import { Table } from './tables/entities/table.entity';
import { TablesModule } from './tables/tables.module';
import { Product } from './products/entities/product.entity';
import { ProductsModule } from './products/products.module';
import { EmployeesModule } from './employees/employees.module';
import { Employee } from './employees/entities/employee.entity';
import { FoodQueuesModule } from './food-queues/food-queues.module';
import { FoodQueue } from './food-queues/entities/food-queue.entity';
import { ReceiptsModule } from './receipts/receipts.module';
import { Receipt } from './receipts/entities/receipt.entity';
import { ServQueuesModule } from './serv-queues/serv-queues.module';
import { ServQueue } from './serv-queues/entities/serv-queue.entity';
import { RolesModule } from './roles/roles.module';
import { Role } from './roles/entities/role.entity';
import { MaterialsModule } from './materials/materials.module';
import { Material } from './materials/entities/material.entity';
import { Order } from './orders/entities/order.entity';
import { OrderItem } from './orders/entities/order-item';
import { OrdersModule } from './orders/orders.module';
import { AuthModule } from './auth/auth.module';
import { DessertsModule } from './desserts/desserts.module';
import { Dessert } from './desserts/entities/dessert.entity';
import { CategoriesModule } from './categories/categories.module';
import { Category } from './categories/entities/category.entity';

@Module({
  imports: [
    TypeOrmModule.forRoot({
      type: 'sqlite',
      database: 'db.sqlite',

      entities: [
        Table,
        Product,
        Employee,
        FoodQueue,
        Receipt,
        ServQueue,
        Role,
        Material,
        Order,
        OrderItem,
        Dessert,
        Category,
      ],

      synchronize: true,
      migrations: [],
    }),
    TablesModule,
    ProductsModule,
    EmployeesModule,
    FoodQueuesModule,
    ReceiptsModule,
    ServQueuesModule,
    RolesModule,
    MaterialsModule,
    OrdersModule,
    AuthModule,
    DessertsModule,
    CategoriesModule,
  ],
  controllers: [AppController],
  providers: [AppService],
})
export class AppModule {
  constructor(private dataSource: DataSource) {}
}
