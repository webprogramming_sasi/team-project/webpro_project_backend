class CreateOrderItemDto {
  productId: number;
  amount: number;
}
export class CreateOrderDto {
  tableId: number;
  orderItems: CreateOrderItemDto[];
}
