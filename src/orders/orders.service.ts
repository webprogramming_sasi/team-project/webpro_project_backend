import { Injectable } from '@nestjs/common';
import { CreateOrderDto } from './dto/create-order.dto';
import { UpdateOrderDto } from './dto/update-order.dto';
import { Repository } from 'typeorm';
import { InjectRepository } from '@nestjs/typeorm';
import { Product } from 'src/products/entities/product.entity';
import { OrderItem } from './entities/order-item';
import { Order } from './entities/order.entity';
import { Employee } from 'src/employees/entities/employee.entity';
import { Table } from 'src/tables/entities/table.entity';

@Injectable()
export class OrdersService {
  constructor(
    @InjectRepository(Order)
    private orderRepository: Repository<Order>,
    @InjectRepository(Table)
    private tableRepository: Repository<Table>,
    @InjectRepository(Product)
    private productRepository: Repository<Product>,
    @InjectRepository(OrderItem)
    private orderItemRepository: Repository<OrderItem>,
    @InjectRepository(Employee)
    private employeeRerository: Repository<Employee>,
  ) {}
  async create(createOrderDto: CreateOrderDto) {
    const table = await this.tableRepository.findOneBy({
      id: createOrderDto.tableId,
    });
    const order: Order = new Order();
    order.table = table;
    order.total = 0;
    await this.orderRepository.save(order);

    const orderItems: OrderItem[] = await Promise.all(
      createOrderDto.orderItems.map(async (od) => {
        const orderItem = new OrderItem();
        orderItem.amount = od.amount;
        orderItem.product = await this.productRepository.findOneBy({
          id: od.productId,
        });

        orderItem.name = orderItem.product.name;
        orderItem.price = orderItem.product.price;
        orderItem.total = orderItem.price * orderItem.amount;
        orderItem.order = order; //อ้างอิงกลับ
        return orderItem;
      }),
    );

    for (const orderItem of orderItems) {
      this.orderItemRepository.save(orderItem);
      order.total = order.total + orderItem.total;
    }

    await this.orderRepository.save(order);
    return await this.orderRepository.findOne({
      where: { id: order.id },
      relations: ['orderItems'],
    });
  }

  findAll() {
    return this.orderRepository.find({ relations: ['table', 'orderItems'] });
  }

  findOne(id: number) {
    return this.orderRepository.findOne({
      where: { id: id },
      relations: ['table', 'orderItems'],
    });
  }

  update(id: number, updateOrderDto: UpdateOrderDto) {
    return `This action updates a #${id} order`;
  }

  async remove(id: number) {
    const order = await this.orderRepository.findOneBy({ id: id });
    return this.orderRepository.softDelete(order);
  }
}
